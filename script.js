"use strict" ;

// 1. Що таке події в JavaScript і для чого вони використовуються?

// Подія - це сигнал того, що щось сталося. Всі вузли DOM генерують такі сигнали
// Події використовуються для створення взаємодії між користувачем і веб-сторінкою, також для відстеження та реагування на зміни в додатку.

// 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.

// mousedown / mouseup: Кнопка миші натискана / відпущена над елементом
// mouseover / mouseout: Вказівник миші наводиться / відходить від елемента.
// mousemove: Кожен рух миші над елементом викликає цю подію.
// click: Спрацьовує після mousedown, а потім mouseup над тим самим елементом, якщо використовувалася ліва кнопка миші.
// dblclick: Спрацьовує після двох кліків по тому самому елементу протягом короткого періоду часу.

// 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

// contextmenu: Спрацьовує, коли натискана права кнопка миші.
// Ця подія надає можливість визначити власну логіку обробки події та відобразити контекстне меню за потреби.

// 1. Додати новий абзац по кліку на кнопку: По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" 
// і додайте його до розділу <section id="content">

document.getElementById('btn-click').addEventListener('click', function() {
    const newParagraph = document.createElement('p');
    newParagraph.textContent = 'New Paragraph';
    document.getElementById('content').appendChild(newParagraph);
  });

//   2. Додати новий елемент форми із атрибутами:
// Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
// По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

const button = document.createElement('button');
button.id = 'btn-input-create';
button.textContent = 'Create Input';

const section = document.querySelector('section');
section.insertBefore(button, section.children[section.children.length - 1].nextSibling);

button.addEventListener('click', () => {
  const input = document.createElement('input');
  input.type = 'text';
  input.placeholder = 'Enter some text here...';
  input.name = 'new-input';

  section.insertBefore(input, button.nextSibling);
});

